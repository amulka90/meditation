package com.example.meditation

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.meditation.databinding.FeelingsListBinding

class FeelingsAdapter(
    private val context: Context,
    private val feelings: List<FeelingsData>
): RecyclerView.Adapter<FeelingsAdapter.ViewHolder>() {
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder(LayoutInflater.from(context).inflate(R.layout.feelings_list, parent, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val binding = FeelingsListBinding.bind(holder.itemView)
        val feeling = feelings[position]


        binding.feelingImage.setImageResource(feeling.image)
        binding.feelingText.text = feeling.title






    }

    override fun getItemCount() = feelings.size
}