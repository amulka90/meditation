package com.example.meditation

data class FeelingsData(
    val image: Int,
    val title: String

)
