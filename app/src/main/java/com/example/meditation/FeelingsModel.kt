package com.example.meditation

data class FeelingsModel(
    val data: List<FeelingsData>
)
