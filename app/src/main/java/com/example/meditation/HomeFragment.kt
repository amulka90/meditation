package com.example.meditation

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.meditation.databinding.FragmentHomeBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class HomeFragment : Fragment(R.layout.fragment_home) {
    private lateinit var binding: FragmentHomeBinding

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding = FragmentHomeBinding.bind(view)

        val  feelingList = binding.feelingsList

        NetworkManager.instance.getFeelings()
            .enqueue(object : Callback<FeelingsModel> {
                override fun onResponse(
                    call: Call<FeelingsModel>,
                    response: Response<FeelingsModel>
                ) {
                    if (response.body() != null) {

                        binding.feelingsList.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL,false)
                        binding.feelingsList.adapter = FeelingsAdapter(requireContext(),response.body()!!.data)


                    }
                }


                override fun onFailure(call: Call<FeelingsModel>, t: Throwable) {
                    t.printStackTrace()
                }
            })
    }
}