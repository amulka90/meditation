package com.example.meditation

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.meditation.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)


        binding.bottomNavigation.setOnNavigationItemSelectedListener { item ->
            if (item.itemId == R.id.homeButton)
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.fragments,HomeFragment())
                    .commit()
            else if (item.itemId == R.id.profileButton)
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.fragments, ProfileFragment())
                    .commit()

            true



        }
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.fragments,HomeFragment())
            .commit()





    }
}