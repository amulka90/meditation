package com.example.meditation

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.meditation.databinding.ActivityOnboardingBinding

class OnboardingActivity : AppCompatActivity() {
    private lateinit var binding: ActivityOnboardingBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityOnboardingBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.onboardingSignButton.setOnClickListener {
            startActivity(
                Intent(this, SignInActivity::class.java)
            )

        }

        binding.registrationButton.setOnClickListener {
            startActivity(
                Intent(this, RegistrationActivity::class.java)
            )

        }
    }
}