package com.example.meditation

data class RegistrationBody(
    val email: String,
    val password: String
)
