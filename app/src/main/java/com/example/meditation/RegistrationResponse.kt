package com.example.meditation

data class RegistrationResponse(
    val id: String,
    val email: String,
    val nickName: String,
    val avatar: String,
    val token: String
)
