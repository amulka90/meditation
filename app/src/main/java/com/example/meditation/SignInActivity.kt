package com.example.meditation

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.meditation.databinding.ActivitySignInBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SignInActivity : AppCompatActivity() {
    private lateinit var binding: ActivitySignInBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySignInBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.signInButton.setOnClickListener {

            val email = binding.emailText.text.toString()
            val password = binding.passwordText.text.toString()

            val user = RegistrationBody(email,password)

            if( email.isNotEmpty() && password.isNotEmpty() && email.contains("@", true)) {
                NetworkManager.instance
                    .registration(user)
                    .enqueue(object : Callback<RegistrationResponse> {
                        override fun onResponse(
                            call: Call<RegistrationResponse>,
                            response: Response<RegistrationResponse>
                        ) {
                            val user = response.body()
                        }

                        override fun onFailure(call: Call<RegistrationResponse>, t: Throwable) {
                            t.printStackTrace()
                        }
                    })
                startActivity(
                    Intent(this, MainActivity::class.java)
                )
                finish()
            }

        }
    }
}